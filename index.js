// [SECTION] John Smith
let firstName = "First Name: John";
console.log(firstName);

let lastName = "Last Name: Smith";
console.log(lastName);

let age = 30;
console.log("age: " + age);

let hobbies = "Hobbies: ";
console.log(hobbies);
let hobbies1 = ["Biking", "Mountain Climbing", "Swimming"];
console.log(hobbies1);


let workAddress = "Work Address: ";
console.log(workAddress);
let workAddress1 = {
	houseNumber: "32",
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}
console.log(workAddress1);



// [SECTION] Steve Rogers
let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let age1 = 40;
console.log("My current age is: " + age1);

let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log("My Friends are: ")
console.log(friends);

let profile = {

	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false

}
console.log("My Full Profile: ")
console.log(profile);

let fullName1 = "Bucky Barnes";
console.log("My bestfriend is: " + fullName1);

let lastLocation = "Atlantic Ocean";
lastLocation = "Arctic Ocean";
console.log("I was found frozen in: " + lastLocation);
